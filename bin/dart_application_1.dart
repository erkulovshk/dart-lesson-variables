void main() {
//задание "Практика с переменными"
//начало

// тест работы на новом пк 06 мая 

  int a = 3;
  double b = 4.20;
  var c = b + a;
  print("b + a = $c");

  String text = 'Name';
  text = 'Flutter';
  print('I want to learn $text');

  double myNumber = 2.225;
  String myText;
  myText = myNumber.toString();
  var dd = myText.runtimeType;
  print("MyText = $myText, myText.runtimeType = $dd");

  // доп. code по окрруглению числе
  double bc = 5.49;
  var rrr = bc.round();
  print('Значение rrr = $rrr,  тип указан ниже');
  print(rrr.runtimeType);

//задание "Практика с переменными"
//конец

//задание "Д/З по переменным и гитхабу"
//начало

// 1
  final String address = 'Suumbaeva st., Bishkek';
  var addressLength = address.length;
  print('Длина address =  $addressLength');

//2
  int age = 20;
  double weight = 63.2;
  String name = 'Daniyar';
  print('Возраст: $age, вес: $weight, имя: $name.');

  // 3

  int myA = 20;
  String myB = '241292';

  myA = int.parse(myB);
  print('myA = $myA');

  //задание "Д/З по переменным и гитхабу"
//конец
}
